from unittest import TestCase

from Calculadora import Calculadora

class TestCalculadora(TestCase):
    def test_sumar(self):
        self.assertEqual(Calculadora().sumar(""),0,"Cadena vacia")

    def test_sumar_una_cadena(self):
        self.assertEqual(Calculadora().sumar("1"),1,"un numero")

    def test_sumar_una_cadena_con_un_numero(self):
        self.assertEqual(Calculadora().sumar("1"),1,"un numero")
        self.assertEqual(Calculadora().sumar("2"),2, "un numero")

    def test_para_sumar_dos_numeros_separados_por_coma(self):
        self.assertEqual(Calculadora().sumar("1,3"),4,"dos numeros")

    def test_para_sumar_dos_numeros_separados_por_coma(self):
        self.assertEqual(Calculadora().sumar("5,2,4,1"),12,"multiples numeros")

    def test_para_sumar_dos_numeros_separados_por_diferentes_separadores(self):
        self.assertEqual(Calculadora().sumar("5:3&2,4:1"),15,"multiples numeros")


